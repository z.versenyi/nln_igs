
import os
import tensorflow as tf
import json
from tensorflow.python.keras.layers import Lambda
import numpy as np
import pandas as pd
from Lib.ilp import Clause
from Lib.ilp import ValuationType
from Lib.ilp_layers import ForwardChainLayer
from Lib.ilp_layers import PredicateLayer
from scipy.stats import truncnorm


def build_model(config, layers):

    vt = config['task'].val_type
    i = tf.keras.Input(shape=vt.shape)
    ILP = ForwardChainLayer(vt, layers)

    os = [i]
    # do = []#added
    for _ in range(config['FC_steps']):
        os += [ILP(os[-1])]
        # do += [tf.keras.layers.Subtract()([os[-1],os[-2]])]#added

    # def f(l):
    #     return tf.stack(l)
    # def g(l):
    #     return 0.3*l[0]+0.7*l[1]
    # Ds = Lambda(f)(do)
    # Vs = Lambda(f)(os[1:])

    # outs = tf.keras.layers.Add()([tf.keras.layers.Multiply()([0.3,D(L)]),tf.keras.layers.Multiply()([0.7,os[-1]])])
    # outs = tf.keras.layers.Multiply()([tf.ones(shape = 1),D(L)])

    # sumo = tf.keras.layers.Add()(os)

    # model = tf.keras.Model(inputs=i, outputs=os[-1])
    model = tf.keras.Model(inputs=i, outputs=os[1:])
    # model = tf.keras.Model(inputs=i, outputs=Vs)
    model.call = tf.contrib.eager.defun(model.call)
    model.print_results = ILP.print_results

    return model

def build_model_2(config, layers):

    vt = config['task'].val_type
    i = tf.keras.Input(shape=vt.shape)
    ILP = ForwardChainLayer(vt, layers)

    def g(l):
        return l[0]+(1.-l[0])*l[1]
    Temp = Lambda(g)

    os = [i]
    # do = []#added
    for _ in range(config['FC_steps']):
        # os += [i+(1-i)*ILP(os[-1])]
        os += [Temp([i,ILP(os[-1])])]
        # do += [tf.keras.layers.Subtract()([os[-1],os[-2]])]#added



    # outs = tf.keras.layers.Add()([tf.keras.layers.Multiply()([0.3,D(L)]),tf.keras.layers.Multiply()([0.7,os[-1]])])
    # outs = tf.keras.layers.Multiply()([tf.ones(shape = 1),D(L)])

    # sumo = tf.keras.layers.Add()(os)

    # model = tf.keras.Model(inputs=i, outputs=os[-1])
    model = tf.keras.Model(inputs=i, outputs=os[1:])
    # model = tf.keras.Model(inputs=i, outputs=Vs)
    model.call = tf.contrib.eager.defun(model.call)
    model.print_results = ILP.print_results

    return model

def build_layers(config):
    program_num_clauses = config['program_num_clauses']
    vt = config['task'].val_type
    num_vars = config['num_vars']

    if 'and_ini' in config.keys():
        and_ini = config['and_ini']
    else:
        and_ini = tf.truncated_normal_initializer(mean=-1,stddev=1)

    if 'or_ini' in config.keys():
        or_ini = config['or_ini']
    else:
        or_ini = tf.truncated_normal_initializer(mean=-1,stddev=1)

    if 'shuffle' in config.keys():
        shuffle = config['shuffle']
    else:
        shuffle = False

    val_types = []
    temp_predars = vt.pred_arity.copy()
    for hp in program_num_clauses.keys():
        val_types+=[ValuationType(temp_predars, vt.constants)]
        # temp_predars = temp_predars.copy()
        # del temp_predars[hp]

    kwargs = {}
    if 'C' in config.keys():
        kwargs['C']=config['C']
        kwargs['v_min']=config['v_min']
        kwargs['v_max']=config['v_max']

    pred_layers = [ PredicateLayer.from_ValuationType(Vt,
                                                        hp,
                                                        num_vars,
                                                        nc,
                                                        name=hp+'_Layer',
                                                        and_ini = and_ini,
                                                        or_ini = or_ini,
                                                        shuffle = shuffle,
                                                        **kwargs
                                                        ) for (hp,nc),Vt in zip(program_num_clauses.items(),val_types)]

    return pred_layers

class ModelState:

    def __init__(self, layers, session = None):
        self.d = {}
        self.preds = {}

        for l in layers:
            hp = l.head_pred
            self.preds[hp]={'pred_arity':l.pred_arity, 'num_vars':l.num_vars}
            if session is None:
                AND = l.AND.w.numpy().tolist()
                OR = l.OR.w.numpy().tolist()
            else:
                AND, OR = session.run((l.AND.w, l.OR.w))
                AND, OR = AND.tolist(), OR.tolist()
            self.d[str(hp)+"_AND"]=AND
            self.d[str(hp)+"_OR"]=OR

    def reset(self, layers, session = None):
        d = self.d
        for l in layers:
            hp = l.head_pred
            if str(hp)+"_AND" in d.keys():
                AND = np.array(d[str(hp)+"_AND"])
                OR = np.array(d[str(hp)+"_OR"])
                if session is None:
                    l.AND.w.assign(AND)
                    l.OR.w.assign(OR)
                else:
                    session.run((l.AND.w.assign(AND),l.OR.w.assign(OR)))
                print("reset weight values for predicate: "+str(hp))

    def save(self, config, fname, comment = None):
        d = {'task_name':config['task'].__class__.__name__,
            'program_num_clauses':config['program_num_clauses'],
            'num_vars':config['num_vars'],
            'comment':comment
            }
        d.update(self.d)

        filepath = "D:/Documents/Python_Scripts/self_d_ILP/v_evocomp/saved_weights/"+fname+".json"
        os.makedirs(os.path.dirname(filepath), exist_ok=True)

        f = open(filepath, 'w')
        json.dump(d,f)
        f.close()

    def log(self, config, fname, dirname, comment = None):
        d = {'task_name':config['task'].__class__.__name__,
            'program_num_clauses':config['program_num_clauses'],
            'num_vars':config['num_vars'],
            'comment':comment
            }
        d.update(self.d)

        filepath = dirname+fname+".json"
        os.makedirs(os.path.dirname(filepath), exist_ok=True)

        f = open(filepath, 'w')
        json.dump(d,f)
        f.close()

    @staticmethod
    def from_file(fname,fullpath = None):
        if not fullpath is None:
            f = open(fullpath)
        else:
            f = open("D:/Documents/Python_Scripts/self_d_ILP/v_evocomp/saved_weights/"+fname+".json")

        d = json.load(f)
        f.close()

        del d['task_name']
        del d['program_num_clauses']
        del d['num_vars']
        del d['comment']

        ret = ModelState([])
        ret.d = d
        ret.preds = None
        return ret

    def print(self, prob = True, dic = None):
        if not dic is None:
            self.preds = dic
        if self.preds is None or self.preds == {}:
            print('Cant print ModelState that was loaded from file. Only ModelState initialized from layers!')
        else:
            for k,w in self.preds.items():
                hp = k
                pred_arity = w['pred_arity']
                num_vars = w['num_vars']

                ors = np.array(self.d[hp+'_OR'])[0]
                ands = np.array(self.d[hp+'_AND'])
                if prob:
                    ors = 1/(1+np.exp(-2*ors))
                    ands = 1/(1+np.exp(-2*ands))
                alist = Clause(hp, pred_arity, num_vars).atom_order

                print('PREDICATE: '+str(hp))

                for i in range(len(ors)):
                    print("")
                    print(ors[i])
                    rest = ""
                    Min = min(ands[i])
                    Max = max(ands[i])
                    for a,v in zip(alist,ands[i]):
                        if prob:
                            if v>0.5:#(Min+Max)/2:
                                rest+=str(v)+": "+str(a)+";    "
                        else:
                            if v>0:#(Min+Max)/2:
                                rest+=str(v)+": "+str(a)+";    "
                    print(rest)
                print("\n\n")

    def print_latex(self, prob = True, dic = None):
        def atstr(a):
            res = ""
            for k in a.args:
                res+=str(k)+","
            return str(a.predicate)+"("+res[:-1]+")"

        if not dic is None:
            self.preds = dic
        if self.preds is None or self.preds == {}:
            print('Cant print ModelState that was loaded from file. Only ModelState initialized from layers!')
        else:
            for k,w in self.preds.items():
                hp = k
                pred_arity = w['pred_arity']
                num_vars = w['num_vars']

                ors = np.array(self.d[hp+'_OR'])[0]
                ands = np.array(self.d[hp+'_AND'])
                if prob:
                    ors = 1/(1+np.exp(-2*ors))
                    ands = 1/(1+np.exp(-2*ands))
                alist = Clause(hp, pred_arity, num_vars).atom_order

                print('PREDICATE: '+str(hp))
                print("\\begin{align*}")
                for i in range(len(ors)):

                    rest = format(ors[i], '.5f')+': target() &\\leftarrow '
                    Min = min(ands[i])
                    Max = max(ands[i])
                    for a,v in zip(alist,ands[i]):
                        if prob:
                            if v>0.5:#(Min+Max)/2:
                                rest+=format(v, '.5f')+": "+atstr(a)+", "
                        else:
                            if v>0:#(Min+Max)/2:
                                rest+=format(v, '.5f')+": "+atstr(a)+", "
                    print(rest[:-2]+"\\\\")
                print("\\end{align*}")

class MutationManager:
    def __init__(self, smoother = 0.9, window = 200):
        self.window = window
        self.losses = []

    def update(self,loss):
        if len(self.losses)==0:
            self.losses = [loss]
        else:
            self.losses+=[loss]

    def can_mutate(self, bottom_cond = 0, max_cond = 20000):

        if len(self.losses)>self.window:
            if self.losses[-1]<bottom_cond:
                return False

            pitch1 = self.find_pitch(self.losses[-self.window:int(-self.window/2):5])/5
            pitch2 = self.find_pitch(self.losses[int(-self.window/2)::5])/5

            if pitch2<pitch1:
                return False

            p = (pitch1+pitch2)/2

            if p<0 and self.losses[-1]/-p>max_cond:
                # print('')
                # print('pitch1: '+str(pitch1))
                # print('pitch2: '+str(pitch2))
                return True


        return False

    @staticmethod
    def find_pitch(lh):
        def f(x,a,b):
            return a*x+b

        xs = list(range(len(lh)))
        ys = lh

        r = np.polyfit(xs, ys, 1)
        return r[0]

class SystemRun():

    def __init__(self, config):

        self.BASE_VAL = None
        self.OFF_CLAUSES = set()
        self.all_clauses = set()


        self.model_config = config['model_config']
        self.task = self.model_config['task']
        self.And_Ini = truncnorm(-2, 2, loc=config['and_mean'], scale=config['and_std'])
        self.Or_Ini = truncnorm(-2, 2, loc=config['or_mean'], scale=config['or_std'])
        self.prune_treshold = config['prune_treshold']
        self.maxepoch = config['maxepoch']
        self.test_freq = config['test_freq']
        self.stuck_epochs = config['stuck_min_epochs']
        self.stop_intermediate = config['stop_cond_intermediate']
        self.stop_final = config['stop_cond_final']
        self.max_iters = config['max_iters']
        self.log_dir = config['log_dir']
        self.log = config['log']

        sess_config=tf.ConfigProto(device_count = {'GPU': 1})
        sess_config.allow_soft_placement = True
        sess_config.gpu_options.allow_growth = True
        sess_config.log_device_placement = False

        self.model_config_spec = self.model_config.copy()
        self.model_config_spec['program_num_clauses']={'target':1}

        self.layers = build_layers(self.model_config)
        model = build_model_2(self.model_config, self.layers)
        self.layer_spec = build_layers(self.model_config_spec)[0]
        model_spec = build_model_2(self.model_config_spec, [self.layer_spec])
        self.layers_dict = {l.head_pred:l for l in self.layers}

        variables_list = model.trainable_weights
        variables_list_spec = [self.layer_spec.AND.w]
        for p in self.model_config['program_num_clauses'].keys():
            self.all_clauses |= {(p,i) for i in range(self.model_config['program_num_clauses'][p])}

        self.BASE_VAL = tf.Variable(self.task.init_Val, trainable = False)

        optimizer = config['opt_init']()

        out_ops = model(self.BASE_VAL)
        if not type(out_ops) is list:
            out_ops = [out_ops]
        self.out_op = out_ops[-1]

        out_ops_spec = model_spec(self.BASE_VAL)
        if not type(out_ops_spec) is list:
            out_ops_spec = [out_ops_spec]
        self.out_op_spec = out_ops_spec[-1]

        self.spec_loss_abs, self.gen_loss_abs = self.loss_absolute(self.out_op, self.task.targ_Val)
        self.spec_loss_full, self.extra_full, self.gen_loss_full = self.loss_base_full(self.out_op, self.task.targ_Val, self.BASE_VAL)
        self.spec_loss_partial, self.extra_partial, self.gen_loss_partial = self.loss_base_partial(self.out_op_spec, self.task.targ_Val, self.BASE_VAL)
        self.spec_loss_fix, self.gen_loss_fix = self.loss_fix(self.out_op, self.task.targ_Val)

        self.abs_loss_op = self.spec_loss_abs + self.gen_loss_abs
        self.full_loss_op = self.spec_loss_full + self.gen_loss_full# + self.extra_full
        self.partial_loss_op = self.spec_loss_partial + self.gen_loss_partial# + self.extra_partial
        self.fix_loss_op = self.spec_loss_fix + self.gen_loss_fix

        gradients_full = optimizer.compute_gradients(self.full_loss_op, var_list=[l.AND.w for l in self.layers])
        self.optimize_full_op = optimizer.apply_gradients(gradients_full)

        gradients_partial = optimizer.compute_gradients(self.partial_loss_op, var_list = variables_list_spec)
        self.optimize_partial_op = optimizer.apply_gradients(gradients_partial)

        # gradients_abs = optimizer.compute_gradients(self.abs_loss_op, var_list=variables_list)
        # self.optimize_abs_op = optimizer.apply_gradients(gradients_abs)

        gradients_fix = optimizer.compute_gradients(self.fix_loss_op, var_list=variables_list)
        self.optimize_fix_op = optimizer.apply_gradients(gradients_fix)

        self.reset_optimizer_op = self.adam_variables_initializer(optimizer,variables_list+variables_list_spec)

        self.session = tf.Session(config=sess_config)

        self.session.run(tf.global_variables_initializer())

        # ModelState(layers, session = session).save(model_config,'latest_run',comment = None)
        # ModelState.from_file('latest_run').reset(layers, session = session)
        # ModelState.from_file('analyse_loss').reset(layers, session = session)
        # ModelState.from_file('manual_save').reset(layers, session = session)

        self.turn_selfs_off()

        #turn spec OR to max
        w = self.session.run(self.layer_spec.OR.w)
        w[0][0]=7
        self.session.run(self.layer_spec.OR.w.assign(w))
        #turn spec self off
        ws_spec = self.session.run([self.layer_spec.AND.w, self.layer_spec.AND.mask])
        h_s = self.layer_spec.get_head_index()
        ws_spec[0][...,h_s] = -7.
        ws_spec[1][...,h_s] = 0
        self.session.run([self.layer_spec.AND.w.assign(ws_spec[0]), self.layer_spec.AND.mask.assign(ws_spec[1])])

        self.BASE = set()
        self.base_loss = 0
        self.steady_iterations = 0
        self.iter_counter = 0

        #full_loss, partial_loss, spec_abs, gen_abs, fix_loss_op
        self.losses = [[],[],[],[],[]]
        self.overview = ""
        # self.generalize_base()

    def run(self):
        fin = False
        while not fin:
            fin = self.generalize_base()

    def adam_variables_initializer(self, adam_opt, var_list):
        adam_vars = [adam_opt.get_slot(var, name)
                     for name in adam_opt.get_slot_names()
                     for var in var_list if var is not None]
        adam_vars.extend(list(adam_opt._get_beta_accumulators()))
        return tf.variables_initializer(adam_vars)

    def loss_absolute(s, output, label):
        #label: (batch_dim, targ_shape) tensor
        #output: (...,batch_dim, val_shape) tensor

        out = tf.gather(s.task.val_type.get_pred(output,'target'),s.task.target_indexes, axis = -1)
        l = tf.gather(s.task.val_type.get_pred(label,'target'),s.task.target_indexes, axis = -1)

        log1 = -tf.math.log(out+tf.keras.backend.epsilon())
        log0 = -tf.math.log(1-out+tf.keras.backend.epsilon())

        spec = tf.reduce_mean((1-l)*log0,-1)
        gen = tf.reduce_mean(l*log1,-1)

        return spec, gen
    def loss_base_full(s, output, label, base):
        #label: (batch_dim, targ_shape) tensor
        #output: (...,batch_dim, val_shape) tensor

        out = tf.gather(s.task.val_type.get_pred(output,'target'),s.task.target_indexes, axis = -1)
        l = tf.gather(s.task.val_type.get_pred(label,'target'),s.task.target_indexes, axis = -1)
        f = tf.gather(s.task.val_type.get_pred(base,'target'),s.task.target_indexes, axis = -1)

        log1 = -tf.math.log(out+tf.keras.backend.epsilon())
        log0 = -tf.math.log(1-out+tf.keras.backend.epsilon())

        # F = tf.reduce_sum(f)
        M = 1-l*(1-f)

        # spec = tf.reduce_sum((f*(1-l)+(1-f)*l)*log0)/F
        # spec = tf.reduce_sum(f*(1-l)*log0)/F
        # gen = tf.reduce_sum(f*l*log1)/F

        # spec = tf.reduce_sum((1-l)*log0)/tf.reduce_sum(M)/2
        spec = tf.reduce_sum((1-l)*log0)/tf.reduce_sum(1-l)/2
        # extra = tf.reduce_sum(l*f*log0)/tf.reduce_sum(M)/2
        gen = tf.reduce_sum((1.-M)*log1)/tf.reduce_sum(1.-M)/2

        # return spec, extra, gen
        return spec, None, gen
    def loss_base_partial(s, output, label, base):
        #label: (batch_dim, targ_shape) tensor
        #output: (...,batch_dim, val_shape) tensor

        out = tf.gather(s.task.val_type.get_pred(output,'target'),s.task.target_indexes, axis = -1)
        l = tf.gather(s.task.val_type.get_pred(label,'target'),s.task.target_indexes, axis = -1)
        b = tf.gather(s.task.val_type.get_pred(base,'target'),s.task.target_indexes, axis = -1)

        log0 = -tf.math.log(1-out+tf.keras.backend.epsilon())
        log1 = -tf.math.log(out+tf.keras.backend.epsilon())
        # log01 = out*log1+(1-out)*log0

        M = l*(1-b)

        # temp = tf.pow(tf.reduce_prod(tf.pow((1.-out),M)),1./tf.reduce_sum(M))
        # temp = tf.reduce_prod(tf.pow((1.-out),M))
        temp = tf.reduce_prod(1.-M*out)#p all ones are false


        gen = -tf.math.log(1-temp+tf.keras.backend.epsilon())/2

        # extra = tf.reduce_sum(l*b*log0)/tf.reduce_sum(1.-M)/2
        # spec = tf.reduce_sum((1-l)*log0)/tf.reduce_sum(1.-M)/2  #this was version2 and 1
        spec = tf.reduce_sum((1-l)*log0)/tf.reduce_sum(1.-l)/2


        # gen1 = tf.reduce_sum(M*((1-out)*log0+out*log1))/tf.reduce_sum(M)/2
        # gen2 = tf.reduce_sum(M*log1)/tf.reduce_sum(M)/2
        # gen = (1-temp)*gen1+temp*gen2

        # return spec, extra, gen
        return spec, None, gen
    def loss_fix(s, output, label):
        #label: (batch_dim, targ_shape) tensor
        #output: (...,batch_dim, val_shape) tensor

        out = tf.gather(s.task.val_type.get_pred(output,'target'),s.task.target_indexes, axis = -1)
        l = tf.gather(s.task.val_type.get_pred(label,'target'),s.task.target_indexes, axis = -1)

        log0 = out
        log1 = -(1-out)*tf.math.log(1-out+tf.keras.backend.epsilon())+1.2*(1-out)

        spec = tf.reduce_mean((1-l)*log0)
        gen = tf.reduce_mean(l*log1)


        # gen1 = tf.reduce_sum(M*((1-out)*log0+out*log1))/tf.reduce_sum(M)/2
        # gen2 = tf.reduce_sum(M*log1)/tf.reduce_sum(M)/2
        # gen = (1-temp)*gen1+temp*gen2

        return spec, gen


    def same_valuation(s, v1, v2, tresh = 0.02):
        return max((v1-v2)*(v1-v2))<tresh*tresh

    def turn_selfs_off(s):
        ws = s.session.run([[s.layers_dict[p].AND.w, s.layers_dict[p].AND.mask] for p in s.layers_dict.keys()])
        ws = dict(zip(s.layers_dict.keys(),ws))

        #self cancelling
        for p, w in ws.items():
            h_i = s.layers_dict[p].get_head_index()
            w[0][...,h_i] = -7.
            w[1][...,h_i] = 0

        s.session.run([[s.layers_dict[p].AND.w.assign(w[0]), s.layers_dict[p].AND.mask.assign(w[1])] for p, w in ws.items()])

    def set_base_val(self, base):
        self.session.run(self.BASE_VAL.assign(self.task.init_Val))
        self.set_active_clauses(base)
        o, spec = self.session.run([self.out_op, self.spec_loss_abs])
        self.session.run(self.BASE_VAL.assign(tf.maximum(o,self.task.init_Val)))
        self.base_loss = spec
        # print("base loss: "+str(self.base_loss))
        return

    def generalize_base(s):

        print("GENERALIZING BASE: "+str(s.BASE))
        s.iter_counter+=1

        if len(s.BASE)==len(s.all_clauses):
            print('No free clauses left!')
            if s.log:
                s.overview+='no free clauses left\n'
            s.done()
            return True

        #train
        c = s.all_clauses-s.BASE
        s.reinit_clauses(c)
        s.set_base_val(s.BASE)
        s.set_active_clauses(c)
        s.session.run(s.reset_optimizer_op)
        MM = MutationManager()

        if s.log:
            ModelState(s.layers, session = s.session).log(s.model_config, 'iter'+str(s.iter_counter)+'_init', s.log_dir, comment = str(s.BASE))
            ao = s.layers[0].atom_order
            w = s.session.run(tf.sigmoid(s.layers[0].AND.w*s.layers[0].AND.c))
            s.overview+="\n\nITERATION: "+str(s.iter_counter)+"\tBASE: "+str(s.BASE)+'\n'
            for p, i in s.BASE:
                s.overview += s.clause_str(w[i], ao, s.layers[0].head_atom)+"\n"
            # s.overview+='\n'

        l_op = s.full_loss_op# s.spec_loss_full+s.gen_loss_full

        for epoch in range(s.maxepoch):
            _, loss, spec, gen = s.session.run([s.optimize_full_op, l_op, s.spec_loss_abs, s.gen_loss_abs])
            MM.update(loss)
            print("epoch: "+str(epoch)+"\tloss: "+str(loss),end = "\r")

            #full_loss, partial_loss, spec_abs, gen_abs, fix_loss_op
            s.losses[0] += [loss]
            s.losses[1] += [None]
            s.losses[2] += [spec]
            s.losses[3] += [gen]
            s.losses[4] += [None]

            if loss<s.stop_intermediate:
                # if s.log:
                #     s.overview+='epoch: '+str(epoch)+'\tloss: '+str(loss)+'\n'
                break
            if epoch % s.test_freq == 0:
                if MM.can_mutate(bottom_cond = 0, max_cond = s.stuck_epochs):
                    # if s.log:
                    #     s.overview+='epoch: '+str(epoch)+'\tloss: '+str(loss)+'\n'
                    break

        if s.log:
            s.overview+='epoch: '+str(epoch)+'\tloss: '+str(loss)+'\n'
            ModelState(s.layers, session = s.session).log(s.model_config, 'iter'+str(s.iter_counter)+'_gen', s.log_dir, comment = str(s.BASE))


        print('\n')
        # ModelState(s.layers, s.session).print()


        #end train



        spec_order = s.prune_and_sort()
        base_changed = False
        # print(spec_order)
        for c in spec_order:
            res = s.specialize_clause(c)
            if res:
                base_changed = True
                s.steady_iterations = 0
                s.BASE = s.BASE|set([c])
                print('\tnew base: '+str(s.BASE))
                if s.log:
                    s.overview+='\tnew base: '+str(s.BASE)+'\n'

        if len(s.BASE)>0 and base_changed:
            res = s.train_and_test_base()
            if res:
                if s.log:
                    s.overview+='\n\nSUCCESS\n'
                s.done()
                return True
        else:
            s.steady_iterations+=1
            if s.steady_iterations>=s.max_iters:
                print('exceeded iteration limit')
                if s.log:
                    s.overview+='exceeded iteration limit\n'
                s.done()
                return True

        # s.generalize_base()
        return False

    def done(self):
        print('')
        print('DONE')
        ModelState(self.layers, self.session).print()

        if self.log:
            ModelState(self.layers, session = self.session).log(self.model_config, 'final', self.log_dir, comment = str(self.BASE))
            ao = self.layers[0].atom_order
            w = self.session.run(tf.sigmoid(self.layers[0].AND.w*self.layers[0].AND.c))
            for p, i in self.BASE:
                self.overview += self.clause_str(w[i], ao, self.layers[0].head_atom)+"\n"

            f = open(self.log_dir+'overview.txt','w')
            f.write(self.overview)
            f.close()

            df = pd.DataFrame(np.array(self.losses).transpose(), columns = ['gen_loss', 'spec_loss', 'spec_real', 'gen_real', 'fix_loss'], dtype = np.float32)
            df.to_csv(self.log_dir+'losses.csv', index=True, index_label = 'epoch', columns = df.columns)

        # for l in self.losses:
        #     plt.plot(l)
        # plt.show()
        self.session.close()
        # tf.reset_default_graph()

    def reinit_clauses(s, clauses):

        preds = set([c[0] for c in clauses])

        np_dict = {}
        for p in preds:
            np_dict[p]=s.session.run([s.layers_dict[p].OR.w,s.layers_dict[p].AND.w])

        for c in clauses:
            np_dict[c[0]][0][0][c[1]]=s.Or_Ini.rvs()
            np_dict[c[0]][1][c[1]]=s.And_Ini.rvs(np_dict[c[0]][1].shape[-1])

        s.session.run([[s.layers_dict[p].OR.w.assign(v[0]), s.layers_dict[p].AND.w.assign(v[1])] for p, v in np_dict.items()])
        s.turn_selfs_off()

    def set_active_clauses(self, clauses):
        clauses = set(clauses)
        allready_off = set(self.OFF_CLAUSES)

        off = self.all_clauses-allready_off-clauses
        on = allready_off&clauses

        self.turn_clauses_on_off(on = on, off = off)
    def turn_clauses_on_off(s, on = [], off = []):

        new_GOC = (s.OFF_CLAUSES|set(off))-set(on)

        #dont turn on ones that are allready turned on
        on = s.OFF_CLAUSES-new_GOC
        #dont turn the ones off that are allready turned off
        off = new_GOC-s.OFF_CLAUSES


        preds_needed = []
        for cl in on|off:
            if not cl[0] in preds_needed:
                preds_needed+=[cl[0]]

        ws = s.session.run([s.layers_dict[p].OR.mask for p in preds_needed])
        ws = dict(zip(preds_needed,ws))

        for pred, ind in on:
            ws[pred][0][ind] = 1.
            s.OFF_CLAUSES-=set([(pred, ind)])

        for pred, ind in off:
            ws[pred][0][ind] = 0.
            s.OFF_CLAUSES|=set([(pred, ind)])

        s.session.run([s.layers_dict[p].OR.mask.assign(w) for p, w in ws.items()])

    def prune_and_sort(s):

        s.set_base_val(set())

        #COMPUTE SHIT
        added = {}
        removed = {}
        for o in s.all_clauses-s.BASE:

            s.set_active_clauses(s.BASE|set([o]))
            res = s.session.run([s.spec_loss_abs, s.gen_loss_abs, s.out_op])
            added[o] = tuple(res)

            s.set_active_clauses(s.all_clauses-set([o]))
            res = s.session.run([s.spec_loss_abs, s.gen_loss_abs, s.out_op])
            removed[o] = tuple(res)

        s.set_active_clauses(s.BASE)
        min_val = s.session.run(s.out_op)
        s.set_active_clauses(s.all_clauses)
        max_val = s.session.run(s.out_op)

        #PRUNE
        #if removing it from the maximum gives the same result as the maximum,
        #and adding them to the minimum gives the same result as just the minimum,
        #then they are useless and we can remove them
        unused = set()

        for c in s.all_clauses-s.BASE:
            if s.same_valuation(removed[c][2], max_val, tresh = s.prune_treshold) and s.same_valuation(added[c][2], min_val, tresh = s.prune_treshold):
                unused|=set([c])

        pruned = s.all_clauses-unused-s.BASE

        #SORT
        b = frozenset(s.BASE)

        specs = [(c, added[c][0]) for c in pruned]
        gens = [(c, added[c][1]) for c in pruned]

        specs.sort(key = lambda t: t[1])
        gens.sort(key = lambda t: t[1])

        indspec = {c[0]:i for c, i in zip(specs, range(len(specs)))}
        indgen = {c[0]:i for c, i in zip(gens, range(len(gens)))}

        scores = [(c, indspec[c]+2*indgen[c]) for c in pruned]
        scores.sort(key = lambda t:t[1])

        return [s[0] for s in scores]

    def specialize_clause(s, c):
        print('\tSPECIALIZE CLAUSE: '+str(c)+'\t WITH BASE: '+str(s.BASE))

        #set clause full on, but don't reinit
        w = s.session.run(s.layers_dict[c[0]].OR.w)
        w[0][c[1]] = 7.
        s.session.run(s.layers_dict[c[0]].OR.w.assign(w))

        if s.log:
            s.overview+='\n\tSPEC '+str(c[0])+' '+str(c[1])+' | '

        #copy from original model
        s.session.run(s.layer_spec.AND.w.assign(s.layers_dict[c[0]].AND.w[c[1]:c[1]+1, ...]))

        s.set_base_val(s.BASE)
        s.set_active_clauses(set([c]))
        s.session.run(s.reset_optimizer_op)
        MM = MutationManager()

        l_op = s.partial_loss_op#s.spec_loss_partial+s.gen_loss_partial #
        last_loss = None
        for epoch in range(s.maxepoch):
            _, loss = s.session.run([s.optimize_partial_op, l_op])
            MM.update(loss)
            last_loss = loss

            #full_loss, partial_loss, spec_abs, gen_abs, fix_loss_op
            s.losses[0] += [None]
            s.losses[1] += [loss]
            s.losses[2] += [None]
            s.losses[3] += [None]
            s.losses[4] += [None]

            print("\tepoch: "+str(epoch)+"\tloss: "+str(loss),end = "\r")
            if loss<s.stop_intermediate:
                # if s.log:
                #     s.overview+='epoch: '+str(epoch)+'  loss: '+str(loss)+'\n'
                break
            if epoch % s.test_freq == 0:
                if MM.can_mutate(bottom_cond = 0, max_cond = s.stuck_epochs):
                    # if s.log:
                    #     s.overview+='epoch: '+str(epoch)+'  loss: '+str(loss)+'\n'
                    break
        print('\n')

        if s.log:
            s.overview+='epoch: '+str(epoch)+'  loss: '+str(loss)+'\n'
            ModelState([s.layer_spec], session = s.session).log(s.model_config_spec, 'iter'+str(s.iter_counter)+'_spec_'+str(c[0])+'_'+str(c[1]), s.log_dir, comment = str(s.BASE))
            ao = s.layer_spec.atom_order
            w = s.session.run(tf.sigmoid(s.layer_spec.AND.w*s.layer_spec.AND.c))
            s.overview += '\t'+s.clause_str(w[0], ao, s.layer_spec.head_atom)+"\n"
        # ModelState(s.layers, s.session).print()

        #copy back to original model
        wres, worig = s.session.run([s.layer_spec.AND.w,s.layers_dict[c[0]].AND.w])
        worig[c[1]] = wres[0]
        s.session.run(s.layers_dict[c[0]].AND.w.assign(worig))

        return last_loss<s.stop_intermediate
        #end train
        # res = train_till_stuck(session, optimize_partial_op, spec_loss_partial+gen_loss_partial, 0.0005, plot_ops = [spec_loss_abs, gen_loss_abs, spec_loss_partial, gen_loss_partial, partial_loss_op], base = base, group = set([c]))
        return

    def train_and_test_base(s):

        print('\t\tTRAINING BASE: '+str(s.BASE))
        # res = train_till_stuck(session, optimize_fix_op, fix_loss_op, 0.0005, plot_ops = [spec_loss_abs, gen_loss_abs, spec_loss_fix, gen_loss_fix], base = set(), group = base)

        s.set_base_val(set())
        s.set_active_clauses(s.BASE)
        s.session.run(s.reset_optimizer_op)
        MM = MutationManager()

        last_loss = None
        for epoch in range(s.maxepoch):
            _, loss, loss_abs, spec, gen = s.session.run([s.optimize_fix_op, s.fix_loss_op, s.abs_loss_op, s.spec_loss_abs, s.gen_loss_abs])
            MM.update(loss)
            last_loss = loss

            print("\t\tepoch: "+str(epoch)+"\tloss: "+str(loss)+"\t\tloss_abs: "+str(loss_abs),end = "\r")

            #full_loss, partial_loss, spec_abs, gen_abs, fix_loss_op
            s.losses[0] += [None]
            s.losses[1] += [None]
            s.losses[2] += [spec]
            s.losses[3] += [gen]
            s.losses[4] += [loss]

            if loss_abs<s.stop_final:
                return True

            if (epoch % s.test_freq == 0):# and loss_abs>s.bottom_cond:
                if MM.can_mutate(bottom_cond = 0, max_cond = s.stuck_epochs):
                    print('\n')
                    # ModelState(s.layers, s.session).print()
                    return False

    def clause_str(self, w, ao, ha):
        ret = str(ha)+" <- "
        for v, a in zip(w, ao):
            if v>0.5:
                ret += str(a)+': '+("%5.3f"%v)+', '
        return ret
