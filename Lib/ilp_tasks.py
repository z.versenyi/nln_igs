from itertools import product
import tensorflow as tf
import numpy as np
from Lib.ilp import atom
import random as rnd
from Lib.ilp import ValuationType

# PARENT CLASS
class ilpTask:

    def __init__(self, rho=0, new_pred_arity = {}):
        iv,tv,ps,cs = self.val_preds_consts(rho)
        t = ValuationType(new_pred_arity,cs)
        # a = tf.zeros(shape = t.shape,dtype = tf.float32)
        a = np.zeros(shape = t.shape)
        # self.init_Val = tf.concat([iv,a],0)
        # self.targ_Val = tf.concat([tv,a],0)
        self.init_Val = np.concatenate([iv,a],axis = 0)
        self.targ_Val = np.concatenate([tv,a],axis = 0)

        ps.update(new_pred_arity)
        self.val_type = ValuationType(ps,cs)

        T = self.val_type.get_pred(self.targ_Val,'target')
        L = int(self.val_type['target'].shape[0])

        self.target_indexes = [k for k in range(L) if int(T[k]) != -1]
        # print(self.target_indexes)
        self.pos_indexes = [k for k in range(L) if int(T[k])==1]
        self.neg_indexes = [k for k in range(L) if int(T[k])==0]

        # self.init_Val = tf.constant(self.init_Val,dtype = tf.float32)
        # self.targ_Val = tf.constant(self.targ_Val,dtype = tf.float32)
        self.init_Val = np.array(self.init_Val,dtype = np.float32)
        self.targ_Val = np.array(self.targ_Val,dtype = np.float32)

    @classmethod
    def val_preds_consts(cls,rho):
        # x = tf.constant(list(cls.val.values()),dtype = tf.float32)
        x = np.array(list(cls.val.values()))
        z = cls.preds.copy()
        w = cls.consts

        if rho == 0:
            # y = tf.constant(list(cls.targ_val.values()),dtype = tf.float32)
            y = np.array(list(cls.targ_val.values()))
        else:
            t_v = cls.targ_val.copy()
            Ps = [n for n, v in t_v.items() if (n.predicate == 'target') and (v  == 1)]
            Ns = [n for n, v in t_v.items() if (n.predicate == 'target') and (v  == 0)]
            cP = rnd.sample(Ps,int(round(rho*len(Ps))))
            cN = rnd.sample(Ns,int(round(rho*len(Ns))))
            for n in cP:
                t_v[n]=0.0
            for n in cN:
                t_v[n]=1.0
            # y = tf.constant(list(t_v.values()),dtype = tf.float32)
            y = np.array(list(t_v.values()))

        return (x,y,z,w)

#TASKS
class predecessorTask(ilpTask):
#PAPER SOLUTION:
#target(x,y)<-succ(y,x)

    consts = [0,1,2,3,4,5,6,7,8,9]
    preds = {'succ':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0]-1==a[1]:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class evenTask(ilpTask):

#RIGHT SOLUTION:
#target(x)<-zero(x)
#target(x)<-targ(z),succ(z,y),succ(y,x)

    consts = [0,1,2,3,4,5]
    preds = {'zero':1, 'succ':2, 'target':1} #only knowlege base and target predicates
    even = [0,2,4]

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in even:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class evenpnTask(ilpTask):

#RIGHT SOLUTION:
#target(x)<-zero(x)
#target(x)<-targ(z),succ(z,y),succ(y,x)
#target(x)<-succ(x,y),succ(y,z),targ(z)


    consts = [-5,-4,-3,-2,-1,0,1,2,3,4,5]
    preds = {'zero':1, 'succ':2, 'target':1} #only knowlege base and target predicates
    even = [-4,-2,0,2,4]

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in even:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class evenabcTask(ilpTask):

#RIGHT SOLUTION:
#target(x)<-zero(x)
#target(x)<-targ(z),succ(z,y),succ(y,x)
#target(x)<-succ(x,y),succ(y,z),targ(z)


    consts = [0,1,2,3,4,5,'a','b','c','d','e','f']
    preds = {'a':1, 'zero':1, 'succ':2, 'target':1} #only knowlege base and target predicates
    succs = [(0,1),(1,2),(2,3),(3,4),(4,5),('a','b'),('b','c'),('c','d'),('d','e'),('e','f')]
    even = [0,2,4,'a','c','e']

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='a':
                if a[0] == 'a':
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if a in succs:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in even:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class lessthanTask(ilpTask):

#RIGHT SOLUTION:
#target(x,y)<-succ(x,y)
#target(x,y)<-target(x,z),target(z,y)

    consts = [0,1,2,3,4,5,6,7,8,9]
    preds = {'succ':2, 'zero':1, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0]<a[1]:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class leqTask(ilpTask):

    #RIGHT SOLUTION:
    #target(x,y)<-succ(x,y)
    #target(x,y)<-succ(x,z),succ(y,z)
    #target(x,y)<-succ(z,x),succ(z,y)
    #target(x,y)<-target(x,z),target(z,y)

    consts = [0,1,2,3,4,5,6,7,8,9]
    preds = {'succ':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0]<=a[1]:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class fizzTask(ilpTask):

#RIGHT SOLUTION:
#-paper:
# target(X)<-zero(X)
# target(X)<-target(Y),pred1(Y,X)
# pred1(X,Y)<-succ(X,Z),pred2(Z,Y)
# pred2(X,Y)<-succ(X,Z),succ(Z,Y)
#
#-my:(needs 4 variables!!)
# target(x)<-zero(x)
# target(x)<-target(y),succ(y,z),succ(z,w),succ(w,x)

    consts = [0,1,2,3,4,5,6]
    preds = {'succ':2, 'zero':1, 'target':1} #only knowlege base and target predicates
    f = [0,3,6]
    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if a[0]+1==a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in f:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class memberTask(ilpTask):
#RIGHT SOLUTION:
#target(X,Y)<-value(Y,X)
#target(X,Y)<-cons(Y,Z),target(X,Z)

    consts = [0,1,2,3,4,5,6,7,8,'a','b','c','d']
    preds = {'value':2, 'cons':2, 'target':2} #only knowlege base and target predicates
    vals = [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(5,'c'),(6,'b'),(7,'c'),(8,'a')]
    con_s = [(1,2),(2,3),(3,4),(4,0),(5,6),(6,7),(7,8),(8,0)]
    targs = [('a',1),('b',1),('c',1),('d',1),
            ('b',2),('c',2),('d',2),
            ('c',3),('d',3),
            ('d',4),
            ('c',5),('b',5),('a',5),
            ('c',6),('b',6),('a',6),
            ('c',7),('a',7),
            ('a',8)]
    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='value':
                if a in vals:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='cons':
                if a in con_s:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targs:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class lengthTask(ilpTask):

#RIGHT SOLUTION:
# -paper:
# target(X,X)<-zero(X)
# target(X,Y)<-cons(X,Z),pred1(Z,Y)
# pred1(X,Y)<-target(X,Z),succ(Z,Y)
# -me:(4 vars)
# target(x,y)<-zero(x),zero(y)
# target(x,y)<-cons(x,z),target(z,u),succ(u,y)


    consts = [0,1,2,3,'a','b','c','d','e','f']
    preds = {'succ':2, 'zero':1, 'cons':2, 'target':2} #only knowlege base and target predicates
    vals = [('a',1),('b',2),('c',3),('d',3),('e',2),('f',0)]
    con_s = [('a','b'),('b','c'),('c',0),('d','e'),('e','f'),('f',0)]
    targs = [('a',3),('b',2),('c',1),
            ('d',3),('e',2),('f',1),
            (0,0)]
    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='zero':
                if a[0] == 0:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='succ':
                if (a[0] in [0,1,2,3,4]) and (a[1] in [0,1,2,3,4,5]) and (a[0]+1==a[1]):
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            # elif p=='value':
            #     if a in vals:
            #         val[atom(p,a)]=1.0
            #         targ_val[atom(p,a)]=1.0
            #     else:
            #         val[atom(p,a)]=0.0
            #         targ_val[atom(p,a)]=0.0
            elif p=='cons':
                if a in con_s:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targs:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class sonTask(ilpTask):

#RIGHT SOLUTION:
#target(x,y)<-father(y,x),brother(x,z)
#target(x,y)<-father(y,x),father(x,z) (this is specified in the paper, but it is not apparent from the data!)


    consts = [0,1,2,3,4,5,6,7,8]
    fathers = [(0,1),(0,2),(3,4),(3,5),(6,7),(6,8)]
    brothers = [(1,2),(2,1),(4,5)]
    sisters = [(5,4),(7,8),(8,7)]
    targets = [(1,0),(2,0),(4,3)]
    preds = {'father':2, 'brother':2, 'sister':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='father':
                if a in fathers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='brother':
                if a in brothers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='sister':
                if a in sisters:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class grandparentTask(ilpTask):

#RIGHT SOLUTION:
# target(X,Y)<-pred1(X,Z),pred1(Z,Y)
# pred1(X,Y)<-father(X,Y)
# pred1(X,Y)<-mother(X,Y)

    consts = [0,1,2,3,4,5,6,7,8]
    fathers = [(0,1),(0,2),(1,3),(1,4)]
    mothers = [(8,0),(2,5),(2,6),(5,7)]
    targets = [(8,1),(8,2),(0,3),(0,4),(0,5),(0,6),(2,7)]
    preds = {'father':2, 'mother':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='father':
                if a in fathers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='mother':
                if a in mothers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class relatednessTask(ilpTask):

#RIGHT SOLUTION: (this is an undirected is_connected task, not a relatedness task)
# target(X,Y)<-pred1(X,Y)
# target(X,Y)<-pred1(X,Z),target(Z,Y)
# pred1(X,Y)<-parent(X,Y)
# pred1(X,Y)<-parent(Y,X)

#simpler:
# target(X,Y)<-parent(X,Y)
# target(X,Y)<-parent(Y,X)
# target(X,Y)<-target(X,Z),target(Z,Y)

    consts = [0,1,2,3,4,5,6,7]
    parents = [(0,1),(0,2),(3,2),(2,4),(2,5),(6,7)]
    target_pos = [(0,0),(0,1),(0,2),(0,4),(0,5),(5,0),(3,1),(7,6)]
    target_neg = [(6,0),(0,7),(4,6),(6,1)]
    preds = {'parent':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='parent':
                if a in parents:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in target_pos:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                elif a in target_neg:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=-1.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class fatherTask(ilpTask):

#RIGHT SOLUTION:
# target(X,Y)<-husband(X,Z),mother(Z,Y)

    consts = [0,1,2,3,4,5,6,7,8,9,10,11]
    husbands = [(1,4),(8,9)]
    mothers = [(4,2),(9,11)]
    brothers = [(1,3),(7,8),(9,10)]
    aunts = [(0,4),(4,5),(4,6)]
    targets = [(1,2),(8,11)]
    preds = {'husband':2, 'mother':2, 'brother':2, 'aunt':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='husband':
                if a in husbands:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='mother':
                if a in mothers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='brother':
                if a in brothers:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='aunt':
                if a in aunts:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class undir_edgeTask(ilpTask):

#RIGHT SOLUTION:
# target(X,Y)<-edge(X,Y)
# target(X,Y)<-edge(Y,X)

    consts = [0,1,2,3,4,5,6,7]
    edges = [(0,1),(1,3),
            (2,2),
            (4,5),(6,7)]
    targets = [(0,1),(1,3),(1,0),(3,1),
            (2,2),
            (4,5),(6,7),(5,4),(7,6)]
    preds = {'edge':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class adj_redTask(ilpTask):

#RIGHT SOLUTION:
# target(x,y)<-edge(x,y),color(y,z),red(z)

    consts = [0,1,2,3,4,5,6,7,8,9,10,11]
    edges = [(0,1),(1,0),(2,3),(2,4),(3,4),
            (6,7),(8,7)]
    colours = [(0,10),(1,11),(2,10),(3,10),(4,11),(5,10),(6,11),(7,10),(8,10),(9,11)]
    targets = [1,2,6,8]
    preds = {'edge':2, 'colour':2, 'red':1, 'green':1, 'target':1} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='colour':
                if a in colours:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='red':
                if a[0]==10:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='green':
                if a[0]==11:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class two_childrenTask(ilpTask):

#RIGHT SOLUTION:
# target(x)<-edge(x,y),edge(x,z),neq(y,z)

    consts = [0,1,2,3,4,5,6,7,8,9]
    edges = [(0,1),(0,2),(1,3),(2,3),(2,4),(3,4),
            (5,6),(6,7),(6,8),(7,7),(7,9),(8,9)]
    targets = [0,2,6,7]
    preds = {'edge':2, 'neq':2, 'target':1} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='neq':
                if a[0]!=a[1]:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class graph_coloringTask(ilpTask):

#RIGHT SOLUTION:
# target(x)<-edge(x,y),color(x,z),color(y,z)

    consts = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
    edges = [(0,1),(1,2),(1,3),(2,4),(4,5),
            (6,7),(7,6),(8,7),(9,8),(10,9),(11,8)]
    R = 12
    G = 13
    colours = [(0,G),(1,R),(2,G),(3,G),(4,R),(5,R),
                (6,G),(7,G),(8,R),(9,G),(10,G),(11,R)]
    targets = [4,6,7,10,11]
    preds = {'edge':2, 'colour':2, 'target':1} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='colour':
                if a in colours:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class graph_connectedTask(ilpTask):

#RIGHT SOLUTION:
# target(X,Y)<-edge(X,Y)
# target(X,Y)<-edge(X,Z),target(Z,Y)

    consts = [0,1,2,3]
    edges = [(0,1),(1,0),(1,2),(2,3)]
    targets = [(0,0),(0,1),(0,2),(0,3),
                (1,0),(1,1),(1,2),(1,3),
                (2,3)]
    preds = {'edge':2, 'target':2} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class cyclicTask(ilpTask):

#RIGHT SOLUTION:
# target(X)<-pred(X,X)
# pred(X,Y)<-edge(X,Y)
# pred(X,Y)<-pred(X,Z),pred(Z,Y)

    consts = [0,1,2,3,4,5,6,7,8,9,10]
    edges = [(0,1),(1,2),(1,3),(2,0),(3,4),(3,5),(4,5),(5,4),
                (6,7),(8,9),(9,10),(10,8)]
    targets = [0,1,2,4,5,8,9,10]
    preds = {'edge':2, 'target':1} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"

class cyclicTask2(ilpTask):

#RIGHT SOLUTION:
# target(X)<-pred(X,X)
# pred(X,Y)<-edge(X,Y)
# pred(X,Y)<-pred(X,Z),pred(Z,Y)

    consts = [0,1,2,3,4,5,6,7,8,9,10]
    edges = [(0,1),(1,2),(2,3),(3,4),(4,5),(4,1),(5,6),(6,7),(7,8),(9,10),(10,9)]
    targets = [1,2,3,4,9,10]
    preds = {'edge':2, 'target':1} #only knowlege base and target predicates

    val = {}
    targ_val = {}
    # filling up initial and target valuations
    for p, arity in preds.items():
        ar = list(product(consts, repeat = arity))
        for a in ar:
            if p=='edge':
                if a in edges:
                    val[atom(p,a)]=1.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            elif p=='target':
                if a[0] in targets:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=1.0
                else:
                    val[atom(p,a)]=0.0
                    targ_val[atom(p,a)]=0.0
            else:
                assert False, "predicate "+p+" is not defined in the initial valuation!"
