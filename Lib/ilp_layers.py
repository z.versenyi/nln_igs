import tensorflow as tf
from tensorflow.keras import layers
from Lib.ilp import Clause
from Lib.ilp import atom
import numpy as np
from itertools import product


class AndLayer(layers.Layer):
    """An and layer in a Neural Logic Network"""

    def __init__(self,
               in_size,
               out_size,
               **kwargs):
        # kwargs: name, and_ini, and_mask

        if not 'name' in kwargs.keys():
            kwargs['name'] = 'AND_LAYER'
        if not 'and_ini' in kwargs.keys():
            m = -1*np.log(1/(1-0.2**(1/in_size))-1)
            kwargs['and_ini'] = tf.truncated_normal_initializer(mean=m,stddev=1)

        super().__init__(name = kwargs['name'])

        self.out_shape = out_size

        # if 'and_mask' in kwargs.keys():
        self.mask = self.add_weight(kwargs['name']+'_m',
                                    shape = [out_size, in_size],
                                    initializer = tf.constant_initializer(1,dtype = tf.float32),
                                    trainable = False)
        #     self.mask.assign(kwargs['and_mask'])
        # else:
        #     self.mask = None

        self.w = self.add_weight(kwargs['name']+'_w',
                                    shape = [out_size, in_size],
                                    initializer = kwargs['and_ini'],
                                    # constraint = lambda x: tf.clip_by_value(x, -5., 5.),
                                    trainable = True)
        self.c = self.add_weight(kwargs['name']+'_c',
                                    shape = [],
                                    initializer = tf.constant_initializer(2.,dtype = tf.float32),
                                    trainable = False)

        # if not self.mask is None:
        #     self.w.assign(20*(self.mask-1)+self.w)

        if 'shuffle' in kwargs.keys():
            self.shuffle = kwargs['shuffle']
        else:
            self.shuffle = False

    def call(self, inputs, training=None):

        #mi = sigmoid(c*wi)
        #conj = PI(1-mi*(1-xi)) = PI(mi*xi + (1-mi)*1)
        #inputs (..., insize)

        # print(inputs.shape)
        x = tf.expand_dims(inputs,-2) #(..., 1, insize)
        # print(x.shape)
        m = tf.sigmoid(self.c*self.w) #(outsize, insize)
        if not self.mask is None:
            m = m*self.mask

        temp = 1.-m*(1.-x) #(..., outsize, insize)

        #alter
        # temp = tf.pow(x,m)#(..., outsize, insize)

        #FOR SHUFFLING TEMP
        # if self.shuffle:
        #     p = tf.range(0,tf.rank(temp),1)
        #     p = tf.concat([p[-1:],p[:-1]],0)
        #     c2 = tf.transpose(temp, perm = p)
        #     c2 = tf.gather(c2, tf.random.shuffle(tf.range(tf.shape(c2)[0])))
        #     # c2 = tf.random_shuffle(c2)
        #     p = tf.range(0,tf.rank(temp),1)
        #     p = tf.concat([p[1:],p[:1]],0)
        #     temp = tf.transpose(c2, perm = p)

        return tf.reduce_prod(temp, axis = -1) #(..., outsize)

    def compute_output_shape(self, input_shape):
        return tf.TensorShape(input_shape[:-1]).concatenate(self.out_shape)

class OrLayer(layers.Layer):
    """An or layer in a Neural Logic Network"""

    def __init__(self,
               in_size,
               out_size,
               **kwargs):
        # kwargs: name, or_ini, or_mask

        if not 'name' in kwargs.keys():
            kwargs['name'] = 'OR_LAYER'
        if not 'or_ini' in kwargs.keys():
            m = -1*np.log(1/(1-0.2**(1/in_size))-1)
            kwargs['or_ini'] = tf.truncated_normal_initializer(mean=m,stddev=1)

        super().__init__(name = kwargs['name'])

        self.out_shape = out_size

        # if 'or_mask' in kwargs.keys():
        self.mask = self.add_weight(kwargs['name']+'_m',
                                    shape = [out_size, in_size],
                                    initializer = tf.constant_initializer(1,dtype = tf.float32),
                                    trainable = False)
        #     self.mask.assign(kwargs['or_mask'])
        # else:
        #     self.mask = None

        self.w = self.add_weight(kwargs['name']+'_w',
                                    shape = [out_size, in_size],
                                    initializer = kwargs['or_ini'],
                                    constraint = lambda x: tf.clip_by_value(x, -10, 10),
                                    trainable = True)

        self.c = self.add_weight(kwargs['name']+'_c',
                                    shape = [],
                                    initializer = tf.constant_initializer(2.,dtype = tf.float32),
                                    trainable = False)

        # if not self.mask is None:
        #     self.w.assign(7*(self.mask-1)+self.w)
        return

    def call(self, inputs, training=None):
        #mi = sigmoid(c*wi)
        #conj = 1-PI(1-mi*xi)
        #inputs (..., insize)

        x = tf.expand_dims(inputs,-2) #(..., 1, insize)

        m = tf.sigmoid(self.c*self.w) #(outsize, insize)
        if not self.mask is None:
            m = m*self.mask

        temp = 1.-m*x #(..., outsize, insize)


        # alter
        # temp = tf.pow((1.-x),m)#(..., outsize, insize)
        return 1.-tf.reduce_prod(temp, axis = -1) #(..., outsize)

    def compute_output_shape(self, input_shape):
        return tf.TensorShape(input_shape[:-1]).concatenate(self.out_shape)

class PredicateLayer(layers.Layer):
    """An AND and an OR layer after each other"""

    def __init__(self,
                head_pred,
                pred_arity,
                numvars,
                num_clauses,
                **kwargs):
        if not 'name' in kwargs.keys():
            kwargs['name'] = head_pred+'_Layer'

        super().__init__(name = kwargs['name'])

        # print("creating predicate layer: "+str(head_pred)+" "+str(pred_arity))

        self.head_pred = head_pred
        self.num_vars = numvars
        self.pred_arity = pred_arity
        self.cl_type = (tuple(self.pred_arity.keys()),numvars)
        weight_shape = int(sum([numvars**a for a in self.pred_arity.values()]))

        self.atom_order = []

        for p, arity in self.pred_arity.items():
            ar = list(product(atom.varnames[:self.num_vars], repeat = arity))
            for a in ar:
                self.atom_order+=[atom(p,a)]

        self.head_atom = atom(self.head_pred,tuple(atom.varnames[:self.pred_arity[self.head_pred]]))

        #magic init
        if ('v_min' in kwargs.keys()) and ('v_max' in kwargs.keys()):
            k_p = np.power(kwargs['C'],self.num_vars-self.pred_arity[self.head_pred])
            o_p = 1.-np.power((1-kwargs['v_max']),1/(k_p*num_clauses))
            num = 1.-np.power((1-kwargs['v_min']),1/(k_p*num_clauses))
            a_p = 1-np.power(num/o_p,1/len(self.atom_order))

            O_p = -np.log((1.-o_p)/o_p)/2
            A_p = -np.log((1.-a_p)/a_p)/2

            kwargs['or_ini'] = tf.constant_initializer(O_p)
            kwargs['and_ini'] = tf.random_uniform_initializer(minval=-7., maxval=A_p)

        if not 'nosublayers' in kwargs:
            kwargs['name'] = head_pred+"_AND"
            self.AND = AndLayer(weight_shape,num_clauses,**kwargs)
            kwargs['name'] = head_pred+"_OR"
            self.OR = OrLayer(num_clauses,1,**kwargs)

    @classmethod
    def from_Clause(cls,
                    clause,
                    num_clauses,
                    **kwargs):
        return cls(clause.head_pred,
                    clause.pred_arity,
                    clause.num_vars,
                    num_clauses,
                    **kwargs)

    @classmethod
    def from_ValuationType(cls,
                            val_type,
                            head_pred,
                            numvars,
                            num_clauses,
                            **kwargs):

        return cls(head_pred,
                    val_type.pred_arity,
                    numvars,
                    num_clauses,
                    **kwargs)

    def call(self, inputs, training=None):
        return self.OR(self.AND(inputs))[...,0]

    def compute_output_shape(self, input_shape):
        return tf.TensorShape(input_shape[:-1])

    def print_results(self):
        ors = self.OR.w.numpy()[0]
        ands = self.AND.w.numpy()
        alist = Clause(self.head_pred, self.pred_arity, self.num_vars).atom_order
        for i in range(len(ors)):
            print("")
            print(ors[i])
            rest = ""
            Min = min(ands[i])
            Max = max(ands[i])
            for a,v in zip(alist,ands[i]):
                if v>(Min+Max)/2:
                    rest+=str(v)+": "+str(a)+";    "
            print(rest)

    def certainty_measure(self):
        #mean(x*(1-x)) the smaller the more certain
        m = tf.sigmoid(self.AND.w*self.AND.c)
        return tf.reduce_mean(-m*tf.math.log(m)/tf.math.log(2.)-(1-m)*tf.math.log(1-m)/tf.math.log(2.), axis = -1)

    def get_head_index(self):
        varnames = atom.varnames
        arity = self.pred_arity[self.head_pred]

        ret = self.get_atom_index(atom(self.head_pred,tuple(varnames[:arity])))
        return ret

    def get_atom_index(self, at):
        ret = self.atom_order.index(at)
        return ret

class ForwardChainLayer(layers.Layer):
    """Takes a full valuation, and does a single forward chain with a list of predicate layers"""

    def __init__(self,
                val_type,
                pred_layers,
                name='FCL',
                **kwargs):
        super().__init__(name=name, **kwargs)
        self.layernames = []
        for l in pred_layers:
            self.layernames+=[l.name]
            setattr(self,l.name,l)
        self.val_type = val_type

    def single_pred_FC(self,valuation,pred_layer):
        #inputs:
        #valuation: (..., val_shape)
        #pred_layer: predicate layer to apply
        #returns tensors of shape (..., forvard_chained_shape)
        self.val_type.assert_shape(valuation)

        SUB_MATRIX = self.val_type.get_sub_matrix(pred_layer.cl_type)
        VALUATION_SUBSTITUTION = Clause.get_val_sub_vect(pred_layer.head_pred,pred_layer.num_vars,self.val_type)

        val_tens = valuation
        # print("val_tens: "+str(val_tens))

        g = tf.gather(val_tens,SUB_MATRIX,axis = -1)
        # print("gethered vals: "+str(g))
        base = pred_layer(g)
        # print("base: "+str(base))
        h = tf.gather(base,VALUATION_SUBSTITUTION,axis = -1)
        # print("gathered base: "+str(h))

        #normal or
        V = 1.-tf.reduce_prod(1.-h,axis = -1)

        # O = self.val_type.get_pred(valuation,pred_layer.head_pred)#No amalgamation
        return V#+O-V*O

    def call(self, inputs, training=None):
        ret = inputs

        # print("input call:")
        # print(ret)

        for ln in self.layernames:
            # print(ln)
            p = getattr(self,ln)
            V = self.single_pred_FC(inputs,p)
            # print(p.head_pred)
            # print(V)
            ret = self.val_type.set_pred(ret,p.head_pred,V)

        # print("return call:")
        # print(ret)
        return ret

    def compute_output_shape(self, input_shape):
        return input_shape

    def print_results(self):
        for ln in self.layernames:
            p = getattr(self,ln)
            print("LAYER: "+str(ln))
            p.print_results()
            print("\n\n")
