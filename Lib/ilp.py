from itertools import product
import tensorflow as tf
import copy

class atom:
    varnames = ['X','Y','Z','U','V','W','K','L','M','N','O','P','Q','R','S','T']
    def __init__(self,predicate,args):
        #args has to be a tuple with hashable objects
        self.args = args
        self.predicate = predicate
    def __eq__(self, other):
        if isinstance(other, atom):
            return (self.predicate == other.predicate) and (self.args == other.args)
        return False
    def __hash__(self):
        return hash((self.predicate, self.args))
    def __str__(self):
        return self.predicate+str(self.args)
    def substitute(self, sub_dict):
        newargs = []
        for arg in self.args:
            if arg in sub_dict.keys():
                newargs+=[sub_dict[arg]]
            else:
                newargs+=[arg]

        return atom(self.predicate,tuple(newargs))

class Clause:

    valuation_substitution_vectors = {}

    def __init__(self,
                    head_pred,
                    pred_arity,
                    numvars,
                    # ini = tf.initializers.random_uniform(0,0.2),
                    wname = None,
                    owner = None):
        self.head_pred = head_pred
        self.pred_arity = pred_arity #these are the predicates that have weights assigned to all atoms
        self.num_vars = numvars
        weight_shape =  int(sum([self.num_vars**a for a in self.pred_arity.values()]))
        # if not owner is None:
        #     self.tensor = owner.add_weight(wname,
        #                                 shape = [weight_shape],
        #                                 initializer = ini,
        #                                 constraint = lambda x: tf.clip_by_value(x, 0., 1.),
        #                                 trainable = True)
        # else:
        #     self.tensor = tf.get_variable(wname,
        #                                 shape = (weight_shape),
        #                                 initializer = ini,
        #                                 constraint = lambda x: tf.clip_by_value(x, 0., 1.),
        #                                 trainable = True)

        self.type = (tuple(self.pred_arity.keys()),self.num_vars)

        varnames = atom.varnames
        self.atom_order = []
        i = 0
        for p, arity in self.pred_arity.items():
            ar = list(product(varnames[:self.num_vars], repeat = arity))
            for a in ar:
                self.atom_order+=[atom(p,a)]
                i+=1

    def __str__(self):
        ret = self.head_pred+" weights:\n"
        for i in range(len(self.atom_order)):
            if self.tensor[i].numpy()>0:
                ret+=str(self.atom_order[i])+":\t\t"+str(self.tensor[i].numpy())+"\n"
        return ret

    def get_val_sub_vect(self,valuation):
        pred_arity = valuation.pred_arity[self.head_pred]
        v = (tuple(valuation.constants), pred_arity)
        if self.num_vars in Clause.valuation_substitution_vectors.keys():
            if v in Clause.valuation_substitution_vectors[self.num_vars].keys():
                return Clause.valuation_substitution_vectors[self.num_vars][v]
        else:
            Clause.valuation_substitution_vectors[self.num_vars]={}

        # print('constructing val_sub_vect: '+str(v)+" "+str(self.num_vars))

        subs = list(product(valuation.constants, repeat = self.num_vars))
        rest_subs = list(product(valuation.constants, repeat = self.num_vars-pred_arity))

        ret = []
        for a in valuation.atom_order:
            p, con = a.predicate, a.args
            if p==self.head_pred:
                l = [subs.index(tuple(con+su)) for su in rest_subs]
                ret+=[l]
        Clause.valuation_substitution_vectors[self.num_vars][v] = ret
        return ret

    @staticmethod
    def get_val_sub_vect(head_pred, num_vars, valuation):
        pred_arity = valuation.pred_arity[head_pred]
        v = (tuple(valuation.constants), pred_arity)
        if num_vars in Clause.valuation_substitution_vectors.keys():
            if v in Clause.valuation_substitution_vectors[num_vars].keys():
                return Clause.valuation_substitution_vectors[num_vars][v]
        else:
            Clause.valuation_substitution_vectors[num_vars]={}

        # print('constructing val_sub_vect: '+str(v)+" "+str(num_vars))

        subs = list(product(valuation.constants, repeat = num_vars))
        rest_subs = list(product(valuation.constants, repeat = num_vars-pred_arity))

        ret = []
        for a in valuation.atom_order:
            p, con = a.predicate, a.args
            if p==head_pred:
                l = [subs.index(tuple(con+su)) for su in rest_subs]
                ret+=[l]
        Clause.valuation_substitution_vectors[num_vars][v] = ret
        return ret

class ValuationType:

    substitution_matrixes = {}
    linkage_matrixes = {}
    atom_orders = {}

    def __init__(self, pred_arity, constants):
        self.pred_arity = pred_arity
        self.predicates = list(pred_arity.keys())
        self.constants = constants
        self.type = (tuple(self.predicates), tuple(self.constants))

        if not self.type in ValuationType.atom_orders.keys():
            # print("generatig atom order: "+str(self.type))
            ao = []
            for p in self.predicates:
                subs = list(product(self.constants, repeat = self.pred_arity[p]))
                for s in subs:
                    at = atom(p,s)
                    ao+=[at]
            ValuationType.atom_orders[self.type]=ao

        self.atom_order = ValuationType.atom_orders[self.type]
        self.shape = (len(self.atom_order),)

    def assert_shape(self, tensor):
        assert self.shape[0] == tensor.shape[-1], "shape of tensor "+str(tensor.shape)+" doesnt match this ValuationType "+str(self.shape)

    def get_pred(self, tensor, pred):
        #get the slice out of tensor, corresponding to the predicate
        self.assert_shape(tensor)
        ar = self.pred_arity[pred]
        c0 = self.constants[0]
        args = [c0 for _ in range(ar)]
        i0 = self.atom_order.index(atom(pred,tuple(args)))
        i1 = i0+len(self.constants)**self.pred_arity[pred]
        return tensor[...,i0:i1]

    def set_pred(self, tensor, pred, var):
        #set the slice in this tensor corresponding to predicate, and return the new tensor
        self.assert_shape(tensor)
        ar = self.pred_arity[pred]
        c0 = self.constants[0]
        args = [c0 for _ in range(ar)]
        i0 = self.atom_order.index(atom(pred,tuple(args)))
        i1 = i0+len(self.constants)**self.pred_arity[pred]
        s1 = tensor[...,i0:i1].shape[-1].value
        assert (s1 == var.shape[-1].value) or (s1 is None) or (var.shape[-1].value is None), "Length of predicate in this ValuationType ("+str(tensor[...,i0:i1].shape[-1])+") doesnt match up with length that is to be substituted ("+str(var.shape[-1])+")"
        return tf.concat([tensor[...,:i0],var,tensor[...,i1:]],axis = -1)

    def __getitem__(self,key):
        return ValuationType({key:self.pred_arity[key]},self.constants)

    def add_pred(self,p,arity):
        pa = self.pred_arity.copy()
        pa[p]=arity
        return ValuationType(pa,self.constants)

    def str(self, tensor):
        self.assert_shape(tensor)
        i=0
        ret = ""
        ret += "Valuation:\n"
        for i in range(len(self.atom_order)):
            if tensor[i]>0:
                ret+=str(self.atom_order[i])+":\t\t"+str(tensor[i])+"\n"
            # i+=1
        return ret

    def get_sub_matrix(self,clause):
        me = self.type
        if type(clause) is Clause:
            cl = clause.type
        else:
            cl = clause

        if me in ValuationType.substitution_matrixes:
            if cl  in ValuationType.substitution_matrixes[me]:
                return ValuationType.substitution_matrixes[me][cl]
        else:
            ValuationType.substitution_matrixes[me]={}

        def contains_repeating(l): #for filtering substitutions with repeating elements
            for i in range(len(l)):
                if l[i] in l[i+1:]:
                    return True
            return False

        # print("constructing new sub_matrix: "+str(me)+" "+str(cl))
        subs = list(product(self.constants, repeat = cl[1]))

        #this is where we filter out subs containing multiple constants ###########################
        # subs = [s for s in subs if not contains_repeating(s)]
        ###########################################################################################

        ret = []
        clause_pred_arities = {key:self.pred_arity[key] for key in cl[0]}
        for s in subs:
            indices = []
            for p, arity in clause_pred_arities.items():
                ar = list(product(s, repeat = arity))
                for a in ar:
                    ind = self.atom_order.index(atom(p,a))
                    indices+=[ind]
            ret+=[indices]
        ValuationType.substitution_matrixes[me][cl] = ret
        return ret
