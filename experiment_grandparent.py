import tensorflow as tf
import Lib.ilp_tasks as ilp_tasks
from Lib.ilp_runner import SystemRun

config = {'model_config':{'program_num_clauses':{'target':4},
                    'task':ilp_tasks.grandparentTask(new_pred_arity = {}),
                    'num_vars':3,
                    'FC_steps':6},
            'and_mean':-1,
            'and_std':1,
            'or_mean':1,
            'or_std':0,
            'opt_init': lambda:tf.train.AdamOptimizer(learning_rate = 0.005, beta1=.9, beta2=.999, epsilon=1e-6, use_locking=False, name='Adam'),
            'prune_treshold':0.05, #maximum absolute difference, under which, valuations are considered equal
            'maxepoch':15000,
            'test_freq':300,
            'stuck_min_epochs': 30000,# if we would reach 0 only after this many epochs, we consider ourselves stuck
            'stop_cond_intermediate':0.001,
            'stop_cond_final':0.0005,
            'max_iters':6,
            'log_dir':"D:/Documents/UU/THESIS/experiments/dummy_test/",
            'log':True
            }

num_runs = 50

for i in range(num_runs):
    print('---------------------------------------------------------')
    print('----------RUN '+str(i)+'\t FROM '+str(num_runs)+'-----------------')
    print('---------------------------------------------------------')
    c = config.copy()
    c['log_dir']+='run_'+str(i)+'/'

    sr = SystemRun(c)
    sr.run()
    del c
    del sr
    tf.reset_default_graph()
